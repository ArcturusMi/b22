#!env/bin/python

import urwid
from g203_led import send_mouse_config

def choice_solid_mode(button, choice):
    ask_color = urwid.Edit(('I say', u"Please enter a color...\n[RRGGBB Hex format]\n"))

    done = urwid.Button(u'Submit')

    resp_body = [
        ask_color, urwid.Divider(), 
        urwid.AttrMap(done, None, focus_map='reversed')
    ]
    urwid.connect_signal(done, 'click', process_color_response, ask_color)
    main.original_widget = urwid.Filler(urwid.Pile(resp_body))

def choice_cycle_mode(button, choice):
    ask_cycle_rate = urwid.Edit(('I say', u"Please enter cycle rate...\n[100-60000ms, default 10000ms]\n"))
    ask_brightness = urwid.Edit(('I say', u"Please enter LED brightness...\n[0-100%, default 100%]\n"))

    done = urwid.Button(u'Submit')

    resp_body = [
        ask_cycle_rate, urwid.Divider(), 
        ask_brightness, urwid.Divider(), 
        urwid.AttrMap(done, None, focus_map='reversed')
    ]
    urwid.connect_signal(done, 'click', process_cycle_response, [ask_cycle_rate, ask_brightness])
    main.original_widget = urwid.Filler(urwid.Pile(resp_body))

def choice_breathing_mode(button, choice):
    ask_color = urwid.Edit(('I say', u"Please enter a color...\n[RRGGBB Hex format]\n"))
    ask_cycle_rate = urwid.Edit(('I say', u"Please enter cycle rate...\n[100-60000ms, default 10000ms]\n"))
    ask_brightness = urwid.Edit(('I say', u"Please enter LED brightness...\n[0-100%, default 100%]\n"))

    done = urwid.Button(u'Submit')

    resp_body = [
        ask_color, urwid.Divider(), 
        ask_cycle_rate, urwid.Divider(), 
        ask_brightness, urwid.Divider(), 
        urwid.AttrMap(done, None, focus_map='reversed'),
    ]
    urwid.connect_signal(done, 'click', process_breathing_response, [ask_color, ask_cycle_rate, ask_brightness])
    main.original_widget = urwid.Filler(urwid.Pile(resp_body))

def menu(title):
    body = [urwid.Divider(), urwid.Text(title), urwid.Divider()]
    
    choices = [
        ('Solid', choice_solid_mode),
        ('Cycle', choice_cycle_mode),
        ('Breathing', choice_breathing_mode),
        ('Off', turn_led_off),
        ('Exit Menu', exit_program),
    ]

    for c in choices:
        mode = c[0]
        callback = c[1]
        choice = generate_choice(mode, callback)
        body.append(urwid.AttrMap(choice, None, focus_map='reversed'))

    return urwid.ListBox(urwid.SimpleFocusListWalker(body))

def generate_choice(name, callback):
    button = urwid.Button(name)
    urwid.connect_signal(button, 'click', callback, 'Solid')
    return button

def process_color_response(button, color):
    send_mouse_config('Solid', [color.edit_text])
    exit_program(button)
    
def process_cycle_response(button, response):
    send_mouse_config('Cycle', [r.edit_text for r in response])
    exit_program(button)

def process_breathing_response(button, response):
    send_mouse_config('Breathing', [r.edit_text for r in response])
    exit_program(button)

def turn_led_off(button, response):
    send_mouse_config('Solid', ['000000'])
    exit_program(button)

def exit_program(button, response=None):
    raise urwid.ExitMainLoop()

if __name__ == "__main__":
    main = urwid.Padding(menu(u'Logitech G102 Prodigy Mouse LED control'), left=2, right=2)
    top = urwid.Overlay(main, urwid.SolidFill(u'\N{MEDIUM SHADE}'),
        align='center', width=('relative', 60),
        valign='middle', height=('relative', 60),
        min_width=20, min_height=9)
    urwid.MainLoop(top, palette=[('reversed', 'standout', '')]).run()