# M-Config 

Aplikasi yang digunakan untuk mengatur LED dari Logitech G102/G203 Prodigy Mouse.

## Requirements

- Python 3.5+
- PyUSB 1.0.2+
- **Root privileges**

## Cara menggunakan Aplikasi
*Note: pastikan Logitech G102/G203 Prodigy Mouse sudah terhubung dengan komputer/laptop

1) Clone repository ini: `git clone https://gitlab.com/ArcturusMi/b22.git`
2) Masuk ke dalam filder B22: `cd b22`
3) Buat virtual environment: `virtualenv ./env`
4) Masuk ke dalam virtual environment: `source env/bin/activate`
5) Install dependencies yang dibutuhkan: `pip install -r requirements.txt`
6) Jalankan aplikasi: `sudo python app.py`

Kelompok B22:
- Doan Andreas Nathanael - 1806205123
- Jonathan - 1806204985
- Muzaki Azami - 1806205470

