#include<stdio.h>
#include<stdlib.h>
#include<errno.h>
#include<fcntl.h>
#include<string.h>
#include<unistd.h>

#define BUFFER_LENGTH 256
static char receive[BUFFER_LENGTH];

int main(){
   int ret, fd;
   char stringToSend[BUFFER_LENGTH];
   printf("Starting device...\n");
   fd = open("/dev/b22_dev", O_RDWR);             
   if (fd < 0){
          perror("Failed to open the device...");
          return errno;
   }
   
   printf("Type in a string to send to the kernel module:\n");
   scanf("%[^\n]%*c", stringToSend);                
   printf("Writing message to the b22_dev [%s].\n", stringToSend);
   ret = write(fd, stringToSend, strlen(stringToSend)); 
   if (ret < 0){
           perror("Failed to write the message to the device.");
           return errno;
   }

 
   printf("Press ENTER to read back from the b22_dev...\n");
   getchar();

   printf("Reading from the device...\n");
   ret = read(fd, receive, BUFFER_LENGTH);        
   if (ret < 0){
          perror("Failed to read the message from the device.");
          return errno;
   }
   printf("The message copied by the device is: [%s]\n", receive);
   return 0;
}

